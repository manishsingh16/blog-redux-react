import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import blogImg from "./icons8-blogger.svg";
import "./header.css";
import {RollbackOutlined} from '@ant-design/icons';
import { Layout, Menu } from 'antd';

const { Header } = Layout;

class Headerr extends Component{
 constructor(props){
  super(props)
  const token = localStorage.getItem("token"); // Authentication logic
  const username=localStorage.getItem("username");
   console.log(token);
        let headerlogged=true;
        if(token==null){
            headerlogged = false;
        }
        this.state = {
            headerlogged,
            username
        }
    console.log(this.state.headerlogged);

 }
    render(){
        return(<>
        <Header className="header" theme="light">
            <div> <img src={blogImg}/></div>
            <div className="header-Blogs"><Link to="/">BLOGS</Link></div>
        {this.state.headerlogged ? <div className="header-logged"> <h3> Welcome:{this.state.username}</h3>
        <div className="header-links"> <Link to="./admin"><RollbackOutlined /></Link> <Link to="/logout">Logout</Link></div></div> : 
         <div className="header-signup"><Link to="./login">Login/SignUp</Link> </div>}
            
        </Header>
         {/* <Header className="header"> 
         <div> <img src={blogImg}/></div> 
        <Menu theme="dark" mode="horizontal">
        <Menu.Item key="1"><Link to="/">Blogs</Link></Menu.Item>
        {this.state.headerlogged ? <Menu.Item key="2"> <h3> Welcome: {this.state.username}</h3> 
         <Link to="./admin"><RollbackOutlined /></Link> <Link to="/logout">Logout</Link>
    </Menu.Item> : 
         <Menu.Item key="2"><Link to='/login' className="header-item2">Sign Up/Login</Link></Menu.Item>}
        
         </Menu>
         <div>helllooooo</div>
       </Header>  */}
        </>);
    }
} 

export default Headerr;

