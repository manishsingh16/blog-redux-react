import {GET_USERS, USERS_ERROR,DELETE_POST} from '../types'
import axios from 'axios'

export const getUsers = () => async dispatch => {
    
    try{
        const res = await axios.get(`https://5fb629f436e2fa00166a4fdc.mockapi.io/blog/blogs`)
        dispatch( {
            type: GET_USERS,
            payload: res.data
        })
    }
    catch(e){
        dispatch( {
            type: USERS_ERROR,
            payload: console.log(e),
        })
    }

}
export const deletePost = (id) => async (dispatch) => {
    const idd = id;
    await axios.delete(`https://5fb629f436e2fa00166a4fdc.mockapi.io/blog/blogs/${idd}`);
    dispatch({
      type: DELETE_POST,
      payload: id,
    });
  };

// http://newsapi.org/v2/top-headlines?country=in&apiKey=093453f7e02049708c0636f508bdf0aa